* [pkgs.alpinelinux.org](https://pkgs.alpinelinux.org/packages?name=procps)

### License
* [procps-ng/procps](https://gitlab.com/procps-ng/procps)@gitlab.com
* [procps](https://packages.debian.org/en/sid/procps)@debian